# ==================================================================
# module list
# ------------------------------------------------------------------
# python        3.6    (apt)
# jupyter       latest (pip)
# pytorch       latest (pip)
# tensorflow    latest (pip)
# ==================================================================

FROM nvidia/cuda:10.0-cudnn7-devel-ubuntu18.04
ENV LANG C.UTF-8
COPY nccl-repo-ubuntu1804-2.4.7-ga-cuda10.0_1-1_amd64.deb /
ADD openmpi-4.0.0.tar.gz /
RUN APT_INSTALL="apt-get install -y --no-install-recommends" && \
    PIP_INSTALL="python -m pip --no-cache-dir install -i https://pypi.douban.com/simple/  --upgrade" && \
    GIT_CLONE="git clone --depth 10" && \
    
    rm -rf /var/lib/apt/lists/* \
           /etc/apt/sources.list.d/cuda.list \
           /etc/apt/sources.list.d/nvidia-ml.list && \

    apt-get update && \

# ==================================================================
# tools
# ------------------------------------------------------------------

    DEBIAN_FRONTEND=noninteractive $APT_INSTALL \
        build-essential \
        apt-utils \
        ca-certificates \
        wget \
        git \
        vim \
        curl \
        unzip \
        unrar \
		nano \
        && \
    git config --global http.proxy && \
    git config --global https.proxy && \
    #$GIT_CLONE  https://github.com/Kitware/CMake ~/cmake && \
    #cd ~/cmake && \
    #./bootstrap && \
	#make -j"$(nproc)" install && \
	
# ==================================================================
# python
# ------------------------------------------------------------------

    DEBIAN_FRONTEND=noninteractive $APT_INSTALL \
        software-properties-common \
        && \
    add-apt-repository ppa:deadsnakes/ppa && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive $APT_INSTALL \
        python3.6 \
        python3.6-dev \
        python3-distutils-extra \
        && \
    wget -O ~/get-pip.py \
        https://bootstrap.pypa.io/get-pip.py && \
    python3.6 ~/get-pip.py && \
    ln -s /usr/bin/python3.6 /usr/local/bin/python3 && \
    ln -s /usr/bin/python3.6 /usr/local/bin/python && \
    $PIP_INSTALL \
        setuptools \
        && \
    $PIP_INSTALL \
        numpy \
        scipy \
        pandas \
        cloudpickle \
        scikit-learn \
        matplotlib \
        Cython \
        plotly \
        nltk \
		xgboost \
		imblearn \
		keras \
		adabound \
        && \

# ==================================================================
# jupyter
# ------------------------------------------------------------------

    $PIP_INSTALL \
        jupyter \
        && \

# ==================================================================
# pytorch
# ------------------------------------------------------------------

    $PIP_INSTALL \
        future \
        numpy \
        protobuf \
        enum34 \
        pyyaml \
        typing \
    	torch \
        && \
    $PIP_INSTALL \
    #"git+https://github.com/pytorch/vision.git" && \
	torchvision && \
    #$PIP_INSTALL \
    #    torch_nightly -f \
    #    https://download.pytorch.org/whl/nightly/cu100/torch_nightly.html \
    #    && \

# ==================================================================
# tensorflow
# ------------------------------------------------------------------

    $PIP_INSTALL \
    #    tf-nightly-gpu-2.0-preview \
		tensorflow-gpu \
        && \
		
# ==================================================================
# nccl
# ------------------------------------------------------------------
	cd / && \
	dpkg -i nccl-repo-ubuntu1804-2.4.7-ga-cuda10.0_1-1_amd64.deb && \
	#apt-get update && \
	#APT_INSTALL libnccl2=2.4.7-1+cuda10.0 libnccl-dev=2.4.7-1+cuda10.0 && \
	
# ==================================================================
# OpenMPI
# ------------------------------------------------------------------
	cd /openmpi-4.0.0 && \
	./configure --prefix=/usr/local && \
	make all install && \
	
	#nano ~/.bashrc && \
	#export PATH=/usr/local/lib:$PATH && \
	#export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH && \
	#source ~/.bashrc && \	

# ==================================================================
# horovod
# ------------------------------------------------------------------
	HOROVOD_GPU_ALLREDUCE=NCCL pip install --no-cache-dir horovod && \
	
	
# ==================================================================
# config & cleanup
# ------------------------------------------------------------------

    ldconfig && \
    apt-get clean && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* ~/*

EXPOSE 8888
